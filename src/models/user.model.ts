import * as uuid from 'uuid/v4';
import { Entity, model, property, hasMany } from '@loopback/repository';
import { Project } from './project.model';
import { Task } from './task.model';
import { TimeEntry } from './time-entry.model';

@model({ settings: { strict: false } })
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    default: () => uuid(),
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
    required: true,
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName: string;

  @hasMany(() => Project, { keyTo: 'userId' })
  projects?: Project[];

  @hasMany(() => Task, { keyTo: 'userId' })
  tasks?: Task[];

  @hasMany(() => TimeEntry, { keyTo: 'userId' })
  timeEntries?: TimeEntry[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}
