import * as uuid from 'uuid/v4';
import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Task } from './task.model';
import { User } from './user.model';

@model({ settings: {} })
export class TimeEntry extends Entity {
  @property({
    type: 'string',
    id: true,
    default: () => uuid(),
  })
  id?: string;

  @property({
    type: 'date',
    required: true,
  })
  start: string;

  @property({
    type: 'number',
    required: true,
  })
  duration: number;

  @property({
    type: 'string',
    required: true,
  })
  type: 'projected' | 'actual';

  @belongsTo(() => Task)
  task: string;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<TimeEntry>) {
    super(data);
  }
}
