import * as uuid from 'uuid/v4';
import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { User } from './user.model';
import { Task } from './task.model';

@model({ settings: { strict: false } })
export class Project extends Entity {
  @property({
    type: 'string',
    id: true,
    default: () => uuid(),
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  color?: string;

  @hasMany(() => Task, { keyTo: 'project' })
  tasks?: Task[];

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<Project>) {
    super(data);
  }
}
