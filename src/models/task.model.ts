import * as uuid from 'uuid/v4';
import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { User } from './user.model';
import { Project } from './project.model';
import { TimeEntry } from './time-entry.model';

@model({ settings: {} })
export class Task extends Entity {
  @property({
    type: 'string',
    id: true,
    default: () => uuid(),
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'date',
    required: true,
  })
  due: string;

  @property({
    type: 'number',
  })
  estimate?: number;

  @hasMany(() => TimeEntry, { keyTo: 'task' })
  timeEntries?: TimeEntry[];

  @belongsTo(() => Project)
  project: string;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<Task>) {
    super(data);
  }
}
