import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Project } from '../models';
import { ProjectRepository, UserRepository } from '../repositories';
import { authenticate, UserProfile, AuthenticationBindings } from '@loopback/authentication';
import { inject, Setter } from '@loopback/core';

export class ProjectController {
  constructor(
    @repository(ProjectRepository)
    public projectRepository: ProjectRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/projects', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Project } } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject('authentication.currentUser') user: UserProfile,
    @requestBody() project: Project,
  ): Promise<Project> {
    delete project.id;
    return await this.userRepository.projects(user.id).create(project);
  }

  @get('/projects/count', {
    responses: {
      '200': {
        description: 'Project model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Project)) where?: Where,
  ): Promise<Count> {
    return await this.projectRepository.count(where);
  }

  @get('/projects', {
    responses: {
      '200': {
        description: 'Array of Project model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Project } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
    @inject('authentication.currentUser') user: UserProfile,
    @param.query.object('filter', getFilterSchemaFor(Project)) filter?: Filter,
  ): Promise<Project[]> {
    return await this.userRepository.projects(user.id).find(filter);
  }

  @patch('/projects', {
    responses: {
      '200': {
        description: 'Project PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody() project: Project,
    @param.query.object('where', getWhereSchemaFor(Project)) where?: Where,
  ): Promise<Count> {
    return await this.projectRepository.updateAll(project, where);
  }

  @get('/projects/{id}', {
    responses: {
      '200': {
        description: 'Project model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Project } } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Project> {
    return await this.projectRepository.findById(id);
  }

  @patch('/projects/{id}', {
    responses: {
      '204': {
        description: 'Project PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
    @requestBody() data: Project,
  ): Promise<void> {
    return this.userRepository.projects(user.id).find({ where: { id } })
      .then(projects => {
        if (projects.length) {
          return this.projectRepository.updateById(id, data);
        }
      })
      .catch(err => console.warn(err));
  }

  @put('/projects/{id}', {
    responses: {
      '204': {
        description: 'Project PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() project: Project,
  ): Promise<void> {
    await this.projectRepository.replaceById(id, project);
  }

  @del('/projects/{id}', {
    responses: {
      '204': {
        description: 'Project DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
  ): Promise<void> {
    await this.userRepository.projects(user.id).delete({ id });
  }
}
