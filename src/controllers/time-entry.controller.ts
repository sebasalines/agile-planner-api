import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { TimeEntry } from '../models';
import { TimeEntryRepository, UserRepository } from '../repositories';
import { inject } from '@loopback/core';
import { UserProfile, authenticate } from '@loopback/authentication';

export class TimeEntryController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(TimeEntryRepository)
    public timeEntryRepository: TimeEntryRepository,
  ) { }

  @post('/time-entries', {
    responses: {
      '200': {
        description: 'TimeEntry model instance',
        content: { 'application/json': { schema: { 'x-ts-type': TimeEntry } } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject('authentication.currentUser') user: UserProfile,
    @requestBody() timeEntry: TimeEntry,
  ): Promise<TimeEntry> {
    return await this.userRepository.timeEntries(user.id).create(timeEntry);
  }

  @get('/time-entries', {
    responses: {
      '200': {
        description: 'Array of TimeEntry model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': TimeEntry } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
    @inject('authentication.currentUser') user: UserProfile,
    @param.query.object('filter', getFilterSchemaFor(TimeEntry)) filter?: Filter,
  ): Promise<TimeEntry[]> {
    return await this.userRepository.timeEntries(user.id).find(filter);
  }

  @patch('/time-entries', {
    responses: {
      '200': {
        description: 'TimeEntry PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async updateAll(
    @inject('authentication.currentUser') user: UserProfile,
    @requestBody() timeEntry: TimeEntry,
    @param.query.object('where', getWhereSchemaFor(TimeEntry)) where?: Where,
  ): Promise<Count> {
    return await this.userRepository.timeEntries(user.id).patch(timeEntry, where);
  }

  @get('/time-entries/{id}', {
    responses: {
      '200': {
        description: 'TimeEntry model instance',
        content: { 'application/json': { schema: { 'x-ts-type': TimeEntry } } },
      },
    },
  })
  @authenticate('jwt')
  async findById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
  ): Promise<TimeEntry> {
    return new Promise((resolve, reject) => {
      this.userRepository.timeEntries(user.id).find({ where: { id } })
        .then(results => {
          if (results.length) {
            resolve(this.timeEntryRepository.findById(id));
          } else {
            resolve(undefined);
          }
        })
        .catch(error => reject(error));
    });
  }

  @patch('/time-entries/{id}', {
    responses: {
      '204': {
        description: 'TimeEntry PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
    @requestBody() timeEntry: TimeEntry,
  ): Promise<void> {
    await this.userRepository.timeEntries(user.id).patch(timeEntry, { id });
  }

  @del('/time-entries/{id}', {
    responses: {
      '204': {
        description: 'TimeEntry DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
  ): Promise<void> {
    await this.userRepository.timeEntries(user.id).delete({ id });
  }
}
