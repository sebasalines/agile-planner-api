import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Task } from '../models';
import { TaskRepository, UserRepository } from '../repositories';
import { authenticate, UserProfile } from '@loopback/authentication';
import { inject } from '@loopback/core';

export class TasksController {
  constructor(
    @repository(TaskRepository)
    public taskRepository: TaskRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/tasks', {
    responses: {
      '200': {
        description: 'Task model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Task } } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject('authentication.currentUser') user: UserProfile,
    @requestBody() task: Task,
  ): Promise<Task> {
    delete task.id; // ensure new uuid from model default
    return await this.userRepository.tasks(user.id).create(task);
  }

  @get('/tasks', {
    responses: {
      '200': {
        description: 'Array of Task model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Task } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
    @inject('authentication.currentUser') user: UserProfile,
    @param.query.object('filter', getFilterSchemaFor(Task)) filter?: Filter,
  ): Promise<Task[]> {
    return await this.userRepository.tasks(user.id).find(filter);
  }

  @patch('/tasks', {
    responses: {
      '200': {
        description: 'Task PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async updateAll(
    @inject('authentication.currentUser') user: UserProfile,
    @requestBody() task: Task,
    @param.query.object('where', getWhereSchemaFor(Task)) where?: Where,
  ): Promise<Count> {
    return await this.userRepository.tasks(user.id).patch(task, where);
  }

  @get('/tasks/{id}', {
    responses: {
      '200': {
        description: 'Task model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Task } } },
      },
    },
  })
  @authenticate('jwt')
  async findById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
  ): Promise<Task> {
    return new Promise((resolve, reject) => {
      this.userRepository.tasks(user.id).find({ where: { id } })
        .then(tasks => {
          if (tasks.length) {
            resolve(this.taskRepository.findById(id));
          } else {
            resolve(undefined);
          }
        })
        .catch(err => reject(err));
    });
  }

  @patch('/tasks/{id}', {
    responses: {
      '204': {
        description: 'Task PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
    @requestBody() task: Task,
  ): Promise<void> {
    await this.userRepository.tasks(user.id).patch(task, { id })
  }

  @del('/tasks/{id}', {
    responses: {
      '204': {
        description: 'Task DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteById(
    @inject('authentication.currentUser') user: UserProfile,
    @param.path.string('id') id: string,
  ): Promise<void> {
    await this.userRepository.tasks(user.id).delete({ id });
  }
}
