import * as _ from 'lodash';
import { inject, Setter } from '@loopback/context';
import {
  AuthenticationBindings,
  UserProfile,
  authenticate,
  TokenService,
  UserService,
} from '@loopback/authentication';
import { get, requestBody, post } from '@loopback/rest';
import { validateCredentials } from '../services/validator';
import { UserRepository, Credentials } from '../repositories';
import { repository } from '@loopback/repository';
import { PasswordHasher } from '../services/hash.password.bcryptjs';
import { PasswordHasherBindings, UserServiceBindings, TokenServiceBindings } from '../keys';
import { User } from '../models';

const CredentialsSchema = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};
export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': { schema: CredentialsSchema },
  },
};

export class AuthController {
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
    @inject.setter(AuthenticationBindings.CURRENT_USER)
    public setCurrentUser: Setter<UserProfile>,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>,
    // @inject(AuthenticationBindings.CURRENT_USER) private user: UserProfile,
  ) { }

  @post('/signUp')
  async signUp(@requestBody() user: User): Promise<User> {
    // ensure a valid email value and password value
    validateCredentials(_.pick(user, ['email', 'password']));

    // encrypt the password
    user.password = await this.passwordHasher.hashPassword(user.password);

    // create the new user
    const savedUser = await this.userRepository.create(user);
    delete savedUser.password;

    return savedUser;
  }

  @post('/users/login')
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{ token: string }> {
    // ensure the user exists, and the password is correct

    const user = await this.userService.verifyCredentials(credentials);

    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);

    return { token };
  }

  @get('/whoami')
  @authenticate('jwt')
  whoAmI(
    @inject('authentication.currentUser') currentUserProfile: UserProfile,
  ): UserProfile {
    return currentUserProfile;
  }
}
