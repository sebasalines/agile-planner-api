import { DefaultCrudRepository, repository, HasManyRepositoryFactory } from '@loopback/repository';
import { User, Project, Task, TimeEntry } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject } from '@loopback/core';
import { ProjectRepository } from './project.repository';
import { TaskRepository } from './task.repository';
import { TimeEntryRepository } from './time-entry.repository';

export type Credentials = {
  email: string;
  password: string;
};

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id
  > {
  public readonly projects: HasManyRepositoryFactory<
    Project,
    typeof Project.prototype.id
  >;
  public readonly tasks: HasManyRepositoryFactory<
    Task,
    typeof Task.prototype.id
  >;
  public readonly timeEntries: HasManyRepositoryFactory<
    TimeEntry,
    typeof TimeEntry.prototype.id
  >;
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @repository(TimeEntryRepository) protected timeEntryRepository: TimeEntryRepository,
    @repository(TaskRepository) protected taskRepository: TaskRepository,
    @repository(ProjectRepository) protected projectRepository: ProjectRepository,
  ) {
    super(User, dataSource);
    this.projects = this.createHasManyRepositoryFactoryFor(
      'projects',
      async () => projectRepository,
    );
    this.tasks = this.createHasManyRepositoryFactoryFor(
      'tasks',
      async () => taskRepository,
    );
    this.timeEntries = this.createHasManyRepositoryFactoryFor(
      'timeEntries',
      async () => timeEntryRepository,
    );
  }
}
