import { DefaultCrudRepository, repository, HasManyRepositoryFactory } from '@loopback/repository';
import { Task, TimeEntry } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject } from '@loopback/core';
import { TimeEntryRepository } from './time-entry.repository';

export class TaskRepository extends DefaultCrudRepository<
  Task,
  typeof Task.prototype.id
  > {
  public readonly timeEntries: HasManyRepositoryFactory<
    TimeEntry,
    typeof TimeEntry.prototype.id
  >;
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @repository(TimeEntryRepository) protected timeEntryRepository: TimeEntryRepository,
  ) {
    super(Task, dataSource);
    this.timeEntries = this.createHasManyRepositoryFactoryFor(
      'timeEntries',
      async () => timeEntryRepository,
    );
  }
}
