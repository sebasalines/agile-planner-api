import {DefaultCrudRepository} from '@loopback/repository';
import {TimeEntry} from '../models';
import {MongodbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TimeEntryRepository extends DefaultCrudRepository<
  TimeEntry,
  typeof TimeEntry.prototype.id
> {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(TimeEntry, dataSource);
  }
}
